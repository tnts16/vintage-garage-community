# Vintage Garage Community Webseite

## Voraussetzungen
- **git** installiert (https://git-scm.com/downloads)
- **npm** installiert (https://nodejs.org/de/download)

## Installation
1. `git clone https://git.thm.de/tnts16/vintage-garage-community.git`: Repository lokal klonen
2. `cd vintage-garage-community`: In Hauptverzeichnis wechseln
3. `npm install`: Alle notwenidgen Packages herunterladen
4. `npm run db_start`: Um die Datenbank zu initialisieren und mit Inhalt zu füllen

## Start
1. `npm start`: Backend starten
2. Im Browser *localhost:4040* aufrufen
