import { Express, NextFunction, Request, Response } from 'express';
import path from 'path';
import {AnmeldeController} from "./AnmeldeController";
import {Benutzer} from "../entities/Benutzer";
import {Lagerhalter} from "../entities/Lagerhalter";
import {Einlagerer} from "../entities/Einlagerer";
import {Stellplatz} from "../entities/Stellplatz";
import {InseratController} from "./InseratController";
import {Service} from "../entities/Service";
import {KFZ} from "../entities/KFZ";
import {Stellplatzbuchung} from "../entities/Stellplatzbuchung";
import {db} from "../server";


export class RoutenController {

  constructor(api: Express) {
    this.setRoutes(api);
  }

  setRoutes(api: Express) {
    // routing for view html pages
    api
      .get("/", (req, res) => {res.sendFile(path.resolve('public/views/index.html'));})
      .get("/login", (req, res) => {res.sendFile(path.resolve('public/views/login/login.html'));})
      .get("/register", (req, res) => {res.sendFile(path.resolve('public/views/register/register.html'));})

      // depositor
      .get("/depositor", this.checkLoginDepositor, (req, res) => {res.sendFile(path.resolve('public/views/depositor/depositor.html'));})
      .get("/show_adverts", (req, res) => {res.sendFile(path.resolve('public/views/depositor/show_adverts.html'));})
      .get("/make_booking", this.checkLoginDepositor, (req, res) => {res.sendFile(path.resolve('public/views/depositor/make_booking.html'));})
      .get("/advert_detail", (req, res) => {res.sendFile(path.resolve('public/views/depositor/advert_detail.html'));})

      // stockkeeper
      .get("/stockkeeper", this.checkLoginStockkeeper, (req, res) => {res.sendFile(path.resolve('public/views/stockkeeper/stockkeeper.html'));})
      .get("/create_advert", this.checkLoginStockkeeper,  (req, res) => {res.sendFile(path.resolve('public/views/stockkeeper/create_advert.html'));})
      .get("/add_services", this.checkLoginStockkeeper, (req, res) => {res.sendFile(path.resolve('public/views/stockkeeper/add_service_to_space.html'));})
      .get("/create_service", this.checkLoginStockkeeper, (req, res) => {res.sendFile(path.resolve('public/views/stockkeeper/create_service.html'));})
      .get("/advert_overview", this.checkLoginStockkeeper,  (req, res) => {res.sendFile(path.resolve('public/views/stockkeeper/advert_overview.html'));})
      .get("/edit_advert", this.checkLoginStockkeeper, (req, res) => {res.sendFile(path.resolve('public/views/stockkeeper/edit_advert.html'));})

      .post("/signIn", this.signIn)
      .post("/signOut", this.checkLogin, this.signOut)
      .post("/registerIn", this.registerIn)
      .get("/unauthorised", (req, res) => {res.sendFile(path.resolve('public/views/unauthorised/unauthorised.html'));})

      // buchung
      .post("/createStellplatzBuchung", this.checkLoginDepositor, this.createBuchung)
      .get("/showOwnBuchung", this.checkLoginDepositor, this.showOwnBuchung)

      // KFZ erstellen
      .post("/createKFZ", this.checkLoginDepositor, this.createKFZ)

      // Stellplätze anzeigen
      .get("/showAdvertlist", this.showAdvertList)
      .get("/showOwnAdvert", this.showOwnAdvert)
      .get("/advertDetail", this.showDetail)

      // Stellplatz erstellen
      .post("/createAdvert", this.checkLoginStockkeeper, this.createStellplatz)
      .post("/createService", this.checkLoginStockkeeper, this.createService)
      
      // error
      .get("*", (req, res) => {res.status(404).sendFile(path.resolve('public/views/unauthorised/error.html'));})
  }

  signIn(req: Request, res: Response) {
    const user = AnmeldeController.benutzerSuchen(req.body.signInName);
    if (user.getNutzername() == 'null') {
      res.sendStatus(404);
    } else if (user.getPasswort() == req.body.signInPass){
      // erfolgsfall -> welche view
      // session key
      const sessionkey: string = Math.random().toString();
      user.setSessionkey(sessionkey);
      req.session.signInName = user.getNutzername();
      req.session.sessionKey = user.getSessionkey();

      if(user instanceof Lagerhalter) {
        // Stockkeeper
        res.status(200).send('STOCKKEEPER');
      } else {
        // Depositor
        res.status(200).send('DEPOSITOR');
      }
    } else {
      res.sendStatus(404);
    }
  }

  registerIn(req: Request, res: Response) {
    let user: Benutzer;
    if (req.body.registerInStockkeeper === null) {
      user = new Einlagerer(req.body.registerInName, req.body.registerInPass);
    } else {
      user = new Lagerhalter(req.body.registerInName, req.body.registerInPass);
    }
    if(AnmeldeController.benutzerHinzufuegen(user)){
      res.sendStatus(200);
    } else {
      res.sendStatus(404);
    }
  }
  checkLoginStockkeeper(req: Request, res: Response, next: NextFunction) {
    const user = AnmeldeController.benutzerSuchen(req.session.signInName);

    if(req.session.signInName !== undefined
        && user.getSessionkey() === req.session.sessionKey
        && user instanceof Lagerhalter) {
      next();
    } else {
      // Client nicht angemeldet
      res.status(401).sendFile(path.resolve('public/views/unauthorised/unauthorised.html'));
    }
  }
  checkLogin(req: Request, res: Response, next: NextFunction) {
    const user = AnmeldeController.benutzerSuchen(req.session.signInName);
    if(req.session.signInName !== undefined
        && user.getSessionkey() === req.session.sessionKey) {
      next();
    } else {
      // Client nicht angemeldet
      res.status(401).sendFile(path.resolve('public/views/unauthorised/unauthorised.html'));
    }
  }
  checkLoginDepositor(req: Request, res: Response, next: NextFunction) {
    const user = AnmeldeController.benutzerSuchen(req.session.signInName);
    if(req.session.signInName !== undefined
        && user.getSessionkey() === req.session.sessionKey
        && user instanceof Einlagerer) {
      next();
    } else {
      // Client nicht angemeldet
      res.status(401).sendFile(path.resolve('public/views/unauthorised/unauthorised.html'));
    }
  }
  signOut(req: Request, res: Response) {
  }
  
  createStellplatz(req: Request, res: Response) {
    // console.log("Test!!!");
    const user: Lagerhalter = AnmeldeController.benutzerSuchen(req.session.signInName) as Lagerhalter;

    // Ausstattungen auswerten -> TODO: in eigene Funktion auslagern?
    let ausstattung: number[] = [];
    if (req.body.advertAusstattung1 !== null) {
      ausstattung.push(1);
    }
    if (req.body.advertAusstattung2 !== null) {
      ausstattung.push(2);
    }
    if (req.body.advertAusstattung3 !== null) {
      ausstattung.push(3);
    }


    for (let i = 0; i < req.body.advertAnzahl; i++) {
      const stellplatz = new Stellplatz(
          user.getNutzername(),
          req.body.advertBezeichnung,
          parseFloat(req.body.advertStadt),
          parseFloat(req.body.advertMiete),
          req.body.advertServiceZeiten,
          req.body.advertZugangsZeiten,
          ausstattung
      );
      // console.log(stellplatz);
      InseratController.stellplatzInseratHinzufuegen(stellplatz);
    }
    res.sendStatus(200);
  }

  createService(req: Request, res: Response){
    const user: Lagerhalter = AnmeldeController.benutzerSuchen(req.session.signInName) as Lagerhalter;
    let service: Service = new Service(req.body.serviceBeschreibung,
        req.body.serviceBezeichnung,
        parseFloat(req.body.serviceMonatlKosten),
        user.getNutzername());
    // console.log(service);
    InseratController.serviceInseratHinzufuegen(service);
  }

  showAdvertList(req: Request, res: Response){
    let stadt: number = parseInt(String(req.query.stadt))
    // console.log(stadt);
    const user: Lagerhalter = AnmeldeController.benutzerSuchen(req.session.signInName) as Lagerhalter;
    // console.log("liste");
    // console.log(JSON.stringify(InseratController.stellplatzInseratSuchenStadt(stadt)));

    const stellplaetze = new Map<Stellplatz, Service[]>();
    let temp: Stellplatz[] = InseratController.stellplatzInseratSuchenStadt(stadt)
    for (let i = 0; i < temp.length; i++) {
      stellplaetze.set(temp[i],InseratController.serviceInseratSuchenLagerhalter(temp[i].getLagerhalter()));
    }
    res.json(Array.from(stellplaetze.entries()));
    //res.json(Object.fromEntries(stellplaetze));

    // let stellplatz = {stellplaetze : InseratController.stellplatzInseratSuchenStadt(stadt),services : InseratController.serviceInseratSuchen()
  }

  createKFZ(req: Request, res: Response) {
    let kfz: KFZ;
    const user: Einlagerer = AnmeldeController.benutzerSuchen(req.session.signInName) as Einlagerer;
    kfz = new KFZ(req.body.beschreibung, parseFloat(req.body.marke), parseFloat(req.body.baujahr), user.getNutzername());
    db.serialize(() => {
      db.get('SELECT nutzer_id FROM Benutzer WHERE nutzername = ?', kfz.getBesitzer(), (err, row) => {
        db.run('INSERT INTO KFZ (bezeichnung, automarke_id, baujahr, einlagerer_id) VALUES (?,?,?,?)',
            kfz.getBezeichnung(),
            kfz.getMarke(),
            kfz.getBaujahr(),
            row.nutzer_id
        );
        db.get('SELECT last_insert_rowid() AS rowid', (err, innerRow) => {
            if (err) {
                console.log(err);
            }
            kfz.setFahrzeugID(parseFloat(innerRow.rowid));
            // console.log(kfz);
            user.setBesitzt(kfz);
            res.json(kfz.getFahrzeugID());
        });
    });
  });

  }

  showDetail(req: Request, res: Response) {
    let id: number = parseInt(String(req.query.id))
    // console.log(id);

    const stellplaetze = new Map<Stellplatz, Service[]>();
    let temp: Stellplatz[] = InseratController.stellplatzInseratSuchenID(id)
    // console.log(temp);
    for (let i = 0; i < temp.length; i++) {
      stellplaetze.set(temp[i],InseratController.serviceInseratSuchenLagerhalter(temp[i].getLagerhalter()));
    }
    res.json(Array.from(stellplaetze.entries()));
  }

  showOwnAdvert(req: Request, res: Response) {
    const user: Lagerhalter = AnmeldeController.benutzerSuchen(req.session.signInName) as Lagerhalter;
    const stellplaetze = new Map<Stellplatz, Service[]>();
    let temp: Stellplatz[] = InseratController.stellplatzInseratSuchenLagerhalter(user);
    for (let i = 0; i < temp.length; i++) {
      stellplaetze.set(temp[i],InseratController.serviceInseratSuchenLagerhalter(temp[i].getLagerhalter()));
    }
    res.json(Array.from(stellplaetze.entries()));
  }

  createBuchung(req: Request, res:Response) {
    const user: Einlagerer = AnmeldeController.benutzerSuchen(req.session.signInName) as Einlagerer;
    let kfz : KFZ = user.getKFZ(parseFloat(req.body.fahrzeug))[0];
    let stellplatzId : number = parseFloat(req.body.stellplatz);
    
    let services : Service[] = [];
    // console.log(req.body.services);
    for (let i = 0; i < req.body.services.length; i++) {
      if (req.body.services[i] != null) {
        services.push(InseratController.serviceInseratSuchenID(parseFloat(req.body.services[i]))[0])
      }
    }

    let buchung : Stellplatzbuchung = new Stellplatzbuchung(
        stellplatzId,
        user.getNutzername(),
        req.body.lagerhalter,
        kfz,
        services);
    // console.log(req.body.stellplatz);
    // console.log(InseratController.stellplatzInseratSuchenID(stellplatzId) as Stellplatz[]);
    InseratController.stellplatzInseratSuchenID(stellplatzId)[0].setStellplatzBuchung(buchung);
    user.setBuchung(buchung);
    // console.log(buchung);
    db.serialize(() => {
      db.get('SELECT nutzer_id FROM Benutzer WHERE nutzername = ?', buchung.getEinlagerer(), (err, einlagererRow) => {
          db.get('SELECT nutzer_id FROM Benutzer WHERE nutzername = ?', buchung.getLagerhalter(), (err, lagerhalterRow) => {
              db.run('INSERT INTO Stellplatzbuchung (datum, stellplatz_id, einlagerer_id, lagerhalter_id, fahrzeug_id) VALUES(?,?,?,?,?)',
                  buchung.getDatum(),
                  buchung.getStellplatzID(),
                  einlagererRow.nutzer_id,
                  lagerhalterRow.nutzer_id,
                  kfz.getFahrzeugID()
              );
              db.get('SELECT last_insert_rowid() AS rowid', (err, innerRow) => {
                  if (err) {
                      console.log(err);
                  }
                  let buchung_id = parseFloat(innerRow.rowid);
                  for (let i = 0;i < buchung.getServices().length; i++) {
                      // console.log(buchung.getServices());
                      let service_id = (buchung.getServices()[i]).getServiceID();
                      db.run('INSERT INTO StellplatzbuchungService(stellplatzbuchung_id, service_id) VALUES(?,?)', buchung_id, service_id);
                  }
              });
          });
      });
    });
    res.sendStatus(200);
  }

  showOwnBuchung(req: Request, res:Response) {
    const user: Einlagerer = AnmeldeController.benutzerSuchen(req.session.signInName) as Einlagerer;
    const stellplaetze = new Map<Stellplatz, Service[]>();
    let temp: Stellplatz[] = []
    for (let j = 0; j < user.getBuchungen().length; j++) {
      temp.push(InseratController.stellplatzInseratSuchenID(user.getBuchungen()[j].getStellplatzID())[0]);
    }
    for (let i = 0; i < temp.length; i++) {
      stellplaetze.set(temp[i],InseratController.serviceInseratSuchenLagerhalter(temp[i].getLagerhalter()));
    }
    res.json(Array.from(stellplaetze.entries()));
  }
}
