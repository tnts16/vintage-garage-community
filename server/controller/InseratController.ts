import {Stellplatz} from "../entities/Stellplatz";
import {Service} from "../entities/Service";
import {Stadt} from "../enums/Stadt";
import {Lagerhalter} from "../entities/Lagerhalter";
import {db} from "../server";


export class InseratController {
    private static stellplatzInserate: Stellplatz[] = [];
    private static serviceInserate: Service[] = [];

    public static setStellplatzInserate(stellplatz: Stellplatz): void {
        this.stellplatzInserate.push(stellplatz);
    }

    public static setServiceInserate(service: Service): void {
        this.serviceInserate.push(service);
    }

    public static stellplatzInseratSuchenID(stellplatzID: number): Stellplatz[] {
        return this.stellplatzInserate.filter((stellplatz) => stellplatz.getStellplatzID() == stellplatzID);
    }

    public static stellplatzInseratSuchenStadt(stadt: Stadt): Stellplatz[] {
        if (stadt == 0) {
            return this.stellplatzInserate;
        }
        let stellplaetze: Stellplatz[] = [];
        for (let i = 0; i < this.stellplatzInserate.length; i++) {
            if (this.stellplatzInserate[i].getStadt() === stadt) {
                stellplaetze.push(this.stellplatzInserate[i]);
            }
        }
        return stellplaetze;
    }

    public static stellplatzInseratSuchenLagerhalter(lagerhalter: Lagerhalter): Stellplatz[] {
        let stellplaetze: Stellplatz[] = [];
        for (let i = 0; i < this.stellplatzInserate.length; i++) {
            if (this.stellplatzInserate[i].getLagerhalter() === lagerhalter.getNutzername()) {
                stellplaetze.push(this.stellplatzInserate[i]);
            }
        }
        return stellplaetze;
    }

    public static serviceInseratSuchenID(serviceID: number): Service[] {
        return this.serviceInserate.filter((service) => service.getServiceID() == serviceID)!;
    }

    public static serviceInseratSuchenLagerhalter(lagerhalter: string): Service[] {
        return this.serviceInserate.filter((service) => service.getLagerhalter() == lagerhalter);
    }

    public static stellplatzInseratHinzufuegen(stellplatz: Stellplatz): void {
        db.serialize(() => {
            db.get(
                'SELECT * FROM Benutzer WHERE nutzername = ?',
                stellplatz.getLagerhalter(),
                (err, row) => {
                    if (err) {
                        console.log(err);
                    }
                    db.run(
                        'INSERT INTO Stellplatz (bezeichnung, stadt, miete, servicezeiten, zugangszeiten, lagerhalter_id) VALUES(?,?,?,?,?,?)',
                        stellplatz.getBezeichnung(),
                        stellplatz.getStadt(),
                        stellplatz.getMiete(),
                        stellplatz.getServiceZeiten(),
                        stellplatz.getZugangsZeiten(),
                        row.nutzer_id
                    );
                    db.get('SELECT last_insert_rowid() AS rowid', (err, innerRow) => {
                        if (err) {
                            console.log(err);
                        }
                        stellplatz.setStellplatzID(innerRow.rowid);
                    });
                    this.stellplatzInserate.push(stellplatz);
                }
            );
        });
    }

    public static serviceInseratHinzufuegen(service: Service): void {
        db.serialize(() => {
            db.get(
                'SELECT nutzer_id FROM Benutzer WHERE nutzername = ?',
                service.getLagerhalter(),
                (err, row) => {
                    if (err) {
                        console.log(err);
                    }
                    db.run(
                        'INSERT INTO Service (name, beschreibung, monatlkosten, lagerhalter_id) VALUES(?,?,?,?)',
                        service.getBezeichnung(),
                        service.getBeschreibung(),
                        service.getMonatlKosten(),
                        row.nutzer_id
                    );
                    db.get('SELECT last_insert_rowid() AS rowid', (err, innerRow) => {
                        if (err) {
                            console.log(err);
                        }
                        service.setServiceID(innerRow.rowid);
                    });
                    this.serviceInserate.push(service);
                }
            );
        });
    }
}