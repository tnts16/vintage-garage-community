import { Benutzer } from "../entities/Benutzer";
import {Lagerhalter} from "../entities/Lagerhalter";
import { db } from "../server";

export class AnmeldeController {
    private static benutzer: Benutzer[] = [];

    public static setBenutzer(benutzer: Benutzer): void {
        this.benutzer.push(benutzer);
    }

    public static benutzerHinzufuegen(benutzer: Benutzer): boolean {
        for (let i = 0; i < this.benutzer.length; i++) {
            if (this.benutzer[i].getNutzername() === benutzer.getNutzername()) {
                return false;
            }
        }

        db.serialize(() => {
            db.run(
                'INSERT INTO Benutzer(nutzername, passwort, nutzertyp) VALUES(?, ?, ?)',
                benutzer.getNutzername(),
                benutzer.getPasswort(),
                benutzer instanceof Lagerhalter ? 0 : 1
            );
        });

        this.benutzer.push(benutzer);
        
        return true;
    }

    public static benutzerSuchen(nutzername: string): Benutzer {
        for (let i = 0; i < this.benutzer.length; i++) {
            if (this.benutzer[i].getNutzername() === nutzername) {
                return this.benutzer[i];
            }
        }
        return this.benutzer[0];
    }
}
