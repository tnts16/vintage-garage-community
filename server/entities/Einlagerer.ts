import {Benutzer} from "./Benutzer";
import {Stellplatzbuchung} from "./Stellplatzbuchung";
import {KFZ} from "./KFZ";

export class Einlagerer extends Benutzer {
    private buchungen: Stellplatzbuchung[];
    private besitzt: KFZ[];

    constructor(nutzername: string, passwort: string) {
        super(nutzername, passwort);
        this.buchungen = [];
        this.besitzt = [];
    }
    
    public getBuchungen(): Stellplatzbuchung[] {
        return this.buchungen;
    }
    
    public setBuchung(buchung: Stellplatzbuchung): void {
        this.buchungen.push(buchung);
    }
    
    public getBesitzt(): KFZ[] {
        return this.besitzt;
    }

    public setBesitzt(kfz: KFZ): void {
        this.besitzt.push(kfz);
    }
    
    public getKFZ(fahrzeugID: number): KFZ[] {
        return this.besitzt.filter((kfz) => kfz.getFahrzeugID() == fahrzeugID);
    }
}