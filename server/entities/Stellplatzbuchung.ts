import {Service} from "./Service";
import {KFZ} from "./KFZ";

export class Stellplatzbuchung {
    private stellplatzID: number;
    private datum: number;
    private einlagerer: string;
    private lagerhalter: string;
    private fahrzeug: KFZ;
    private services: Service[];

    constructor(stellplatzId: number, einlagerer: string, lagerhalter: string, fahrzeug: KFZ, service: Service[]) {
        this.datum = Date.now();
        this.stellplatzID = stellplatzId;
        this.einlagerer = einlagerer;
        this.lagerhalter = lagerhalter;
        this.fahrzeug = fahrzeug;
        this.services = service;
    }

    public getStellplatzID(): number {
        return this.stellplatzID;
    }

    public getServices(): Service[] {
        return this.services;
    }

    public getDatum(): number {
        return this.datum;
    }

    public getLagerhalter(): string {
        return this.lagerhalter;
    }

    public getEinlagerer(): string {
        return this.einlagerer;
    }
}