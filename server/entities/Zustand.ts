export class Zustand {
    private fahrbereit: boolean;
    private wartungFaellig: string;
    private beschreibung: string;

    constructor(fahrbereit: boolean, wartungFaellig: string, beschreibung: string) {
        this.fahrbereit = fahrbereit;
        this.wartungFaellig = wartungFaellig;
        this.beschreibung = beschreibung;
    }
}