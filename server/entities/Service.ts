export class Service {
    private serviceID: number;
    private bezeichnung: string;
    private beschreibung: string;
    private monatlKosten: number;
    private lagerhalter: string;

    constructor(bezeichnung: string, beschreibung: string, monatlKosten: number, lagerhalter: string) {
        this.serviceID = 0;
        this.bezeichnung = bezeichnung;
        this.beschreibung = beschreibung;
        this.monatlKosten = monatlKosten;
        this.lagerhalter = lagerhalter;
    }

    public getServiceID(): number {
        return this.serviceID;
    }

    public getBezeichnung(): string {
        return this.bezeichnung;
    }

    public getBeschreibung(): string {
        return this.beschreibung;
    }

    public getMonatlKosten(): number {
        return this.monatlKosten;
    }

    public getLagerhalter(): string {
        return this.lagerhalter;
    }

    public setServiceID(serviceID: number): void {
        this.serviceID = serviceID;
    }
}