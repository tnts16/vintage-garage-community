import {Stadt} from "../enums/Stadt";

export abstract class Benutzer {
  private nutzername: string;
  private passwort: string;
  private sessionkey: string;
  private vorname: string;
  private nachname: string;
  private email: string;
  private wohnort: Stadt;

  constructor(nutzername: string, passwort: string) {
    this.nutzername = nutzername;
    this.passwort = passwort;
    this.sessionkey = "";
    this.wohnort = 0;
    this.email = "";
    this.vorname = "";
    this.nachname = "";
  }

  public getNutzername(): string {
    return this.nutzername;
  }
  
  public getPasswort(): string {
    return this.passwort;
  }

  public setSessionkey(sessionkey: string): void {
    this.sessionkey = sessionkey;
  }

  public getSessionkey(): string {
    return this.sessionkey;
  }
  
  public profilBearbeiten(vorname: string, nachname: string, email: string, wohnort: Stadt): void {
    this.vorname = vorname;
    this.nachname = nachname;
    this.email = email;
    this.wohnort = wohnort;
  }
}
