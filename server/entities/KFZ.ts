import {Automarke} from "../enums/Automarke";
import {Zustand} from "./Zustand";

export class KFZ {
    private fahrzeugID: number;
    private bezeichnung: string;
    private marke: Automarke;
    private baujahr: number;
    private besitzer: string;
    private zustand: Zustand | null;

    constructor(bezeichnung: string, marke: Automarke, baujahr: number, besitzer: string) {
        this.fahrzeugID = 0;
        this.zustand = null;
        this.baujahr = baujahr;
        this.bezeichnung = bezeichnung;
        this.marke = marke;
        this.besitzer = besitzer;
    }

    public getFahrzeugID(): number {
        return this.fahrzeugID;
    }

    public getBezeichnung(): string {
        return this.bezeichnung;
    }

    public getMarke(): Automarke {
        return this.marke;
    }

    public getBaujahr(): number {
        return this.baujahr;
    }

    public getBesitzer(): string {
        return this.besitzer;
    }

    public setFahrzeugID(fahrzeugID: number): void {
        this.fahrzeugID = fahrzeugID;
    }

    public getZustand(): Zustand | null {
        return this.zustand;
    }

    public setZustand(zustand: Zustand): void {
        this.zustand = zustand;
    }
}