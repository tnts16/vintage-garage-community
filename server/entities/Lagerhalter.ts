import {Benutzer} from "./Benutzer";
import {Automarke} from "../enums/Automarke";
import {Stadt} from "../enums/Stadt";

export class Lagerhalter extends Benutzer {
    private spezialisierungAufAutomarken: Automarke[];
    private firmensitz: Stadt;
    private informiert: boolean;

    public informieren() : void {
        this.informiert = true;
    }
    
    public angesehen() : void {
        this.informiert = false;
    }
    
    public lagerProfilBearbeiten(vorname: string, nachname: string, email: string, wohnort: Stadt, firmensitz: Stadt, spezialisierung: Automarke[]): void {
        super.profilBearbeiten(vorname, nachname, email, wohnort);
        this.spezialisierungAufAutomarken = spezialisierung;
        this.firmensitz = firmensitz;
    }

    constructor(nutzername: string, passwort: string) {
        super(nutzername, passwort);
        this.spezialisierungAufAutomarken = []
        this.firmensitz = 0;
        this.informiert = false;
    }
}

