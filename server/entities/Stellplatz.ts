import {Stadt} from "../enums/Stadt";
import {Stellplatzbuchung} from "./Stellplatzbuchung";
import {Ausstattung} from "../enums/Ausstattung";

export class Stellplatz {
    private stellplatzID: number;
    private bezeichnung: string;
    private stadt: Stadt;
    private miete: number;
    private serviceZeiten: string;
    private zugangsZeiten: string;
    private lagerhalter: string;
    private buchung: Stellplatzbuchung | null;
    private ausstattung: Ausstattung[];

    constructor(lagerhalter: string,bezeichnung: string, stadt: Stadt, miete: number, serviceZeiten: string, zugangsZeiten: string, ausstattung: Ausstattung[]) {
        this.stellplatzID = 0;
        this.bezeichnung = bezeichnung;
        this.stadt = stadt;
        this.miete = miete;
        this.serviceZeiten = serviceZeiten;
        this.zugangsZeiten = zugangsZeiten;
        this.ausstattung = ausstattung;
        this.lagerhalter = lagerhalter;
        this.buchung = null;
    }

    public getStellplatzID(): number {
        return this.stellplatzID;
    }

    public getBezeichnung(): string {
        return this.bezeichnung;
    }

    public getStadt(): Stadt {
        return this.stadt;
    }

    public getMiete(): number {
        return this.miete;
    }

    public getServiceZeiten(): string {
        return this.serviceZeiten;
    }

    public getZugangsZeiten(): string {
        return this.zugangsZeiten;
    }

    public getLagerhalter(): string {
        return this.lagerhalter;
    }

    public getStellplatzBuchung(): Stellplatzbuchung | null {
        return this.buchung;
    }

    public setStellplatzBuchung(buchung: Stellplatzbuchung): void {
        this.buchung = buchung;
    }

    public setStellplatzID(stellplatzID: number): void {
        this.stellplatzID = stellplatzID;
    }
}
