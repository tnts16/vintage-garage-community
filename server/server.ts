import express, {Express} from 'express';
import session from "express-session";
import {RoutenController} from "./controller/RoutenController";
import sqlite from "sqlite3";
import { AnmeldeController } from './controller/AnmeldeController';
import { Benutzer } from './entities/Benutzer';
import { Lagerhalter } from './entities/Lagerhalter';
import { Einlagerer } from './entities/Einlagerer';
import { Stellplatz } from './entities/Stellplatz';
import { InseratController } from './controller/InseratController';
import {Ausstattung} from "./enums/Ausstattung";
import {Service} from "./entities/Service";
import {KFZ} from "./entities/KFZ";
import { Stellplatzbuchung } from './entities/Stellplatzbuchung';
import path from 'path';

const api: Express = express();
const port = 4040;

// Ergänzt/Überlädt den Sessionstore um das Attribut "signInName"
declare module "express-session" {
    interface Session {
        signInName: string;
        sessionKey: string;
    }
}

// open a new data base connection and export it
export const db = new sqlite.Database('garage.db');

// initialisiere Benutzer in AnmeldeController
db.serialize(() => { 
    db.each('select * from Benutzer', (err, row) => {
      let benutzer: Benutzer;
      if (row.nutzertyp == 0) {
          benutzer = new Lagerhalter(row.nutzername, row.passwort);
      } else {
          benutzer = new Einlagerer(row.nutzername, row.passwort);
      }
      AnmeldeController.setBenutzer(benutzer);
    });

    // initialisiere Fahrzeuge, Servicebuchungen
    db.each(`select * from KFZ INNER JOIN Benutzer ON KFZ.einlagerer_id = Benutzer.nutzer_id`, (err, kfzRow) => {
        const einlagerer = AnmeldeController.benutzerSuchen(kfzRow.nutzername) as Einlagerer;
        let kfz : KFZ = new KFZ(
            kfzRow.bezeichnung,
            kfzRow.automarke_id,
            kfzRow.baujahr,
            einlagerer.getNutzername()
        )
        kfz.setFahrzeugID(parseFloat(kfzRow.kfz_id));
        einlagerer.setBesitzt(kfz);

    });

    // initialisiere Stellplätze in InseratControlle
    db.each(`select * from Stellplatz AS s INNER JOIN Benutzer AS b ON s.lagerhalter_id = b.nutzer_id`, (err, row) => {
        let ausstattungen: Ausstattung[] = [];
        db.each(`select * from StellplatzAusstattung WHERE stellplatz_id = ?`,
            row.stellplatz_id,
            (err, innerRow) => {
                ausstattungen.push(innerRow.ausstattung_id as number);
            });
        let stellplatz = new Stellplatz(
            row.nutzername,
            row.bezeichnung,
            row.stadt,
            row.miete,
            row.servicezeiten,
            row.zugangszeiten,
            ausstattungen
        );
        stellplatz.setStellplatzID(row.stellplatz_id);
        InseratController.setStellplatzInserate(stellplatz);
    });

    // initialisiere Services in InseratController
    db.each(`select * from Service AS s INNER JOIN Benutzer AS b ON s.lagerhalter_id = b.nutzer_id`, (err, row) => {
        let service = new Service(
            row.name,
            row.beschreibung,
            row.monatlkosten,
            row.nutzername
        );
        service.setServiceID(row.service_id);
        InseratController.setServiceInserate(service);
    });


    // initialisiere Stellplatzbuchungen
    db.each(`select e.nutzername AS enutzername, l.nutzername AS lnutzername, stellplatz_id, fahrzeug_id, stellplatzbuchung_id
            from (Stellplatzbuchung AS s INNER JOIN Benutzer AS e ON s.einlagerer_id = e.nutzer_id)
            INNER JOIN Benutzer AS l ON s.lagerhalter_id = l.nutzer_id`, (err, row) => {

            let einlagerer = AnmeldeController.benutzerSuchen(row.enutzername) as Einlagerer;
            let services: Service[] = [];

            db.each(`select * from StellplatzbuchungService as sbs INNER JOIN Stellplatzbuchung AS sb ON sbs.stellplatzbuchung_id = ?`, row.stellplatzbuchung_id, (err, sbsRow) => {
                let service = InseratController.serviceInseratSuchenID(sbsRow.service_id)[0];
                if (services.find((serv) => serv == service)) {

                } else {
                    services.push(service);
                }
            });
        const stellplatzbuchung = new Stellplatzbuchung(
            row.stellplatz_id,
            row.enutzername,
            row.lnutzername,
            einlagerer.getKFZ(parseFloat(row.fahrzeug_id))[0],
            services
        );
        // console.log(stellplatzbuchung);
        const stellplatz = InseratController.stellplatzInseratSuchenID(row.stellplatz_id)[0];
        stellplatz.setStellplatzBuchung(stellplatzbuchung);
        einlagerer.setBuchung(stellplatzbuchung);
    });
});

api.listen(port, () => {
  console.log(`⚡️[server]: Server is running at http://localhost:${port}`);
});

// Bei jedem Request werden vorhandene Nutzdaten von Text in Objekte geparst
api.use(express.json());
api.use(express.urlencoded({extended: false}));

api.use(express.static('./'));

api.use(
  session({
    cookie: { expires: new Date(Date.now() + (1000 * 60 * 60)) },
    secret: Math.random().toString(),
      resave: true,
      saveUninitialized: true
  })
);

const routenController = new RoutenController(api);
