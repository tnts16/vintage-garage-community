const sqlite3 = require('sqlite3').verbose();
const db = new sqlite3.Database('garage.db');

db.serialize(() => {

    db.run(`INSERT INTO Benutzer(nutzername, passwort, nutzertyp) VALUES ("dieter", "d", 0)`);
    db.run('INSERT INTO Benutzer(nutzername, passwort, nutzertyp) VALUES ("s", "s", 0)');
    db.run('INSERT INTO Benutzer(nutzername, passwort, nutzertyp) VALUES ("d", "d", 1)');

    db.run(`INSERT INTO Benutzer(nutzername, passwort, nutzertyp, nachname, vorname, email, wohnort_id, firmensitz_id) VALUES(
        'maxmu65',
        'passwort',
        0,
        'Mustermann',
        'Max',
        'maxmustermann@vintage-garage.de',
        4,
        3
    )`);
    db.run(`INSERT INTO Benutzer(nutzername, passwort, nutzertyp, nachname, vorname, email, wohnort_id) VALUES(
        'erika83',
        '12345',
        1,
        'Mustermann',
        'Erika',
        'erika83@mail.com',
        2
    )`);
    db.run(`INSERT INTO Benutzer(nutzername, passwort, nutzertyp, nachname, vorname, email, wohnort_id, firmensitz_id) VALUES(
        'mark.helmke',
        'qwertz',
        0,
        'Helmke',
        'Mark',
        'mark.helmke@private.de',
        6,
        6
    )`);

    db.run(`INSERT INTO Stellplatz(bezeichnung, stadt, miete, servicezeiten, zugangszeiten, lagerhalter_id) VALUES(
        'Schattiger Einzelstellplatz',
        1,
        49.99,
        'Mo.-Fr.: 8.00 Uhr-17.00 Uhr',
        'Mo.-Fr.: 8.00 Uhr-17.00 Uhr',
        1
    )`);
    db.run(`INSERT INTO Stellplatz(bezeichnung, stadt, miete, servicezeiten, zugangszeiten, lagerhalter_id) VALUES(
        'Parkplatz in privater Tiefgarage',
        3,
        40.00,
        'Mo.-Sa.: 8.00 Uhr-13.00 Uhr',
        'Mo.-Sa.: 8.00 Uhr-13.00 Uhr',
        1
    )`);
    db.run(`INSERT INTO Stellplatz(bezeichnung, stadt, miete, servicezeiten, zugangszeiten, lagerhalter_id) VALUES(
        'Parkplatz in privater Tiefgarage',
        3,
        40.00,
        'Mo.-Sa.: 8.00 Uhr-13.00 Uhr',
        'Mo.-Sa.: 8.00 Uhr-13.00 Uhr',
        1
    )`);
    db.run(`INSERT INTO Stellplatz(bezeichnung, stadt, miete, servicezeiten, zugangszeiten, lagerhalter_id) VALUES(
        'Erhöhter Ausstellungsplatz',
        2,
        85.00,
        'Mo.-So.: 6.00 Uhr-20.00 Uhr',
        'Mo.-So.: 6.00 Uhr-20.00 Uhr',
        1
    )`);
    db.run(`INSERT INTO Stellplatz(bezeichnung, stadt, miete, servicezeiten, zugangszeiten, lagerhalter_id) VALUES(
        'Abgetrennter Bereich auf öffentlichem Parkplatz',
        6,
        29.99,
        'Mo.-Do.: 9.00 Uhr-17.30 Uhr',
        'Mo.-Do.: 9.00 Uhr-17.30 Uhr',
        4
    )`);
    db.run(`INSERT INTO Stellplatz(bezeichnung, stadt, miete, servicezeiten, zugangszeiten, lagerhalter_id) VALUES(
        'Abgetrennter Bereich auf öffentlichem Parkplatz',
        6,
        29.99,
        'Mo.-Do.: 9.00 Uhr-17.30 Uhr',
        'Mo.-Do.: 9.00 Uhr-17.30 Uhr',
        4
    )`);
    db.run(`INSERT INTO Stellplatz(bezeichnung, stadt, miete, servicezeiten, zugangszeiten, lagerhalter_id) VALUES(
        'Abgetrennter Bereich auf öffentlichem Parkplatz',
        6,
        29.99,
        'Mo.-Do.: 9.00 Uhr-17.30 Uhr',
        'Mo.-Do.: 9.00 Uhr-17.30 Uhr',
        4
    )`);
    db.run(`INSERT INTO Stellplatz(bezeichnung, stadt, miete, servicezeiten, zugangszeiten, lagerhalter_id) VALUES(
        'Privater Parkplatz auf eingezäuntem Gelände',
        2,
        59.99,
        'Sa.-So.: 9.00 Uhr-15.30 Uhr',
        'Sa.-So.: 9.00 Uhr-15.30 Uhr',
        4
    )`);

    db.run(`INSERT INTO Service(name, beschreibung, monatlkosten, lagerhalter_id) VALUES(
        'Glasbox',
        'Ihr Fahrzeug wird in einer Glasbox gelagert, sodass Sie an Wochenenden mit Ihren Freunden bei einem Bier Ihren ausgestellten Oldtimer bewundern können.', 
        149.99,
        1
    )`);
    db.run(`INSERT INTO Service(name, beschreibung, monatlkosten, lagerhalter_id) VALUES(
        'Einschweißen',
        'Ihr Fahrzeug wird in einer aus nachhaltigen Rohstoffen hergestellten Folie eingeschweißt.', 
        79.99,
        1
    )`);
    db.run(`INSERT INTO Service(name, beschreibung, monatlkosten, lagerhalter_id) VALUES(
        'Autowäsche',
        'Ihr Fahrzeug wird einmal wöchentlich gewaschen.', 
        30.00,
        4
    )`);
    db.run(`INSERT INTO Service(name, beschreibung, monatlkosten, lagerhalter_id) VALUES(
        'Abholung & Lieferung',
        '', 
        120.00,
        4
    )`);
    db.run(`INSERT INTO Zustand(fahrbereit, wartungfaellig, beschreibung) VALUES(
        true,
        '2023-01-16 10:00:00.000', 
        'Guter Zustand'
    )`);
    db.run(`INSERT INTO Zustand(fahrbereit, wartungfaellig, beschreibung) VALUES(
        false,
        '2022-09-26 15:30:00.000', 
        'Totalschaden'
    )`);
    db.run(`INSERT INTO Stellplatzbuchung(stellplatz_id, fahrzeug_id, datum, einlagerer_id, lagerhalter_id) VALUES(
        1,
        1,
        '2022-09-26 15:30:00.000',
        5,
        1
    )`);
    db.run(`INSERT INTO Stellplatzbuchung(stellplatz_id, fahrzeug_id, datum, einlagerer_id, lagerhalter_id) VALUES(
        2,
        2,
        '2022-09-26 15:30:00.000',
        3,
        4
    )`);
    db.run(`INSERT INTO StellplatzAusstattung(stellplatz_id, ausstattung_id) VALUES(
        1,
        1
    )`);
    db.run(`INSERT INTO StellplatzAusstattung(stellplatz_id, ausstattung_id) VALUES(
        1,
        2
    )`);
    db.run(`INSERT INTO StellplatzAusstattung(stellplatz_id, ausstattung_id) VALUES(
        2,
        2
    )`);
    db.run(`INSERT INTO StellplatzAusstattung(stellplatz_id, ausstattung_id) VALUES(
        3,
        1
    )`);
    db.run(`INSERT INTO StellplatzAusstattung(stellplatz_id, ausstattung_id) VALUES(
        3,
        3
    )`);
    db.run(`INSERT INTO StellplatzAusstattung(stellplatz_id, ausstattung_id) VALUES(
        5,
        3
    )`);
    db.run(`INSERT INTO StellplatzAusstattung(stellplatz_id, ausstattung_id) VALUES(
        6,
        1
    )`);
    db.run(`INSERT INTO KFZ(bezeichnung, automarke_id, baujahr, einlagerer_id, zustand_id) VALUES(
        'Herbie',
        3,
        1963,
        5,
        1
    )`);
    db.run(`INSERT INTO KFZ(bezeichnung, automarke_id, baujahr, einlagerer_id, zustand_id) VALUES(
        '911',
        1,
        1984,
        3,
        2
    )`);
    db.run(`INSERT INTO StellplatzbuchungService(stellplatzbuchung_id, service_id) VALUES(
        1,
        1
    )`);
    db.run(`INSERT INTO StellplatzbuchungService(stellplatzbuchung_id, service_id) VALUES(
        1,
        2
    )`);
    db.run(`INSERT INTO StellplatzbuchungService(stellplatzbuchung_id, service_id) VALUES(
        2,
        3
    )`);

});

db.close();