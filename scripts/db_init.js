const sqlite3 = require('sqlite3').verbose();
const db = new sqlite3.Database('garage.db');

db.serialize(() => {
  // Benutzer
  db.run('DROP TABLE IF EXISTS Benutzer');
  db.run(`CREATE TABLE Benutzer(
    nutzer_id INTEGER PRIMARY KEY AUTOINCREMENT,
    nutzername STRING,
    passwort STRING,
    sessionkey STRING,
    vorname STRING,
    nachname STRING,
    email STRING,
    wohnort_id INTEGER,
    firmensitz_id INTEGER,
    informiert BOOLEAN,
    nutzertyp INTEGER
  )`);
 
  // Stellplatz
  db.run('DROP TABLE IF EXISTS Stellplatz');
  db.run(`CREATE TABLE Stellplatz(
    stellplatz_id INTEGER PRIMARY KEY AUTOINCREMENT,
    bezeichnung STRING,
    stadt INTEGER,
    miete FLOAT,
    servicezeiten STRING,
    zugangszeiten STRING,
    lagerhalter_id INTEGER
  )`);
  
  // Service
  db.run('DROP TABLE IF EXISTS Service');
  db.run(`CREATE TABLE Service(
    service_id INTEGER PRIMARY KEY AUTOINCREMENT,
    name STRING,
    beschreibung STRING,
    monatlkosten FLOAT,
    lagerhalter_id INTEGER
  )`);
  
  // Zustand
  db.run('DROP TABLE IF EXISTS Zustand');
  db.run(`CREATE TABLE Zustand(
    zustand_id INTEGER PRIMARY KEY AUTOINCREMENT,
    fahrbereit BOOLEAN,
    wartungfaellig DATE,
    beschreibung STRING
  )`);

  // Stellplatzbuchung
  db.run('DROP TABLE IF EXISTS Stellplatzbuchung');
  db.run(`CREATE TABLE Stellplatzbuchung(
    stellplatzbuchung_id INTEGER PRIMARY KEY AUTOINCREMENT,
    datum DATE,
    stellplatz_id INTEGER,
    einlagerer_id INTEGER,
    lagerhalter_id INTEGER,
    fahrzeug_id INTEGER
  )`);

  // StellplatzbuchungService
  db.run('DROP TABLE IF EXISTS StellplatzbuchungService');
  db.run(`CREATE TABLE StellplatzbuchungService(
    stellplatzbuchung_id INTEGER,
    service_id INTEGER,
    PRIMARY KEY (stellplatzbuchung_id, service_id)
  )`);
  
  // StellplatzAusstattung
  db.run('DROP TABLE IF EXISTS StellplatzAusstattung');
  db.run(`CREATE TABLE StellplatzAusstattung(
    stellplatz_id INTEGER,
    ausstattung_id INTEGER,
    PRIMARY KEY (stellplatz_id, ausstattung_id)
  )`);

  // KFZ
  db.run('DROP TABLE IF EXISTS KFZ');
  db.run(`CREATE TABLE KFZ(
    kfz_id INTEGER PRIMARY KEY AUTOINCREMENT,
    bezeichnung STRING,
    automarke_id INTEGER,
    baujahr INTEGER,
    einlagerer_id INTEGER,
    zustand_id INTEGER
  )`);
});

db.close();
