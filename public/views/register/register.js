//const axios = require("axios");

let registerComp;
let alertComp;

document.addEventListener("DOMContentLoaded", () => {
    registerComp = document.getElementById("register");
    document.getElementById("registerForm").addEventListener("submit", register);
});

function register(event) {
    event.preventDefault();

    const target = event.currentTarget;
    const formData = new FormData(target);

    let Depositor = null;
    let Stockkeeper = null;

    console.log(formData.get("radio_role"));
    if("Stockkeeper" == formData.get("radio_role")){
        Stockkeeper = "on";
    }
    else if("Depositor" == formData.get("radio_role")){
        Depositor = "on";
    }

    axios.post("/registerIn", {
        registerInName: formData.get("registerInName"),
        registerInPass: formData.get("registerInPass"),
        registerInStockkeeper: Stockkeeper,
        registerInDepositor: Depositor
    }).then(() => {
        const userName = formData.get("registerInName").toString();
        target.reset();
        alert("Registrierung des Nutzers " + userName + " erfolgreich");
    }).catch((err) => {
        switch (err.response.status) {
            case 404: //Not found
                alert("Nutzername bereits vergeben");
                break;
            default: //Sonstige Fehler
                alert("ALLES KAPUTT");
                break;
        }
    });
}

// Gibt Fehlermeldungen für 10 Sekunden auf der Seite aus
function printAlert(msg) {
    alertComp.innerHTML = `<p class="text-danger">${msg}</p>`
    setTimeout(() => {
        hide(alertComp);
    }, 10000);
    show(alertComp);
}

function show(elem) {
    elem.style.display = "block";
}

function hide(elem) {
    elem.style.display = "none";
}