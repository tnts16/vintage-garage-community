// const axios = require("axios");

let signInComp;
let alertComp;

document.addEventListener("DOMContentLoaded", () => {
    signInComp = document.getElementById("signIn");
    document.getElementById("signInForm").addEventListener("submit", signIn);

});

function signIn(event) {
    event.preventDefault();
    const target = event.currentTarget;
    const formData = new FormData(target);
    axios.post("/signIn", {
        signInName: formData.get("signInName"),
        signInPass: formData.get("signInPass")
    }).then((value) => {
        if (value.data === 'DEPOSITOR') {
            window.location = "/depositor"
        } else {
            window.location = "/stockkeeper"
        }
    }).catch((err) => {
        switch (err.response.status) {
            case 404: //Not found
                alert("Benutzername oder Passwort ungültig");
                break;
            default: //Sonstige Fehler
                alert("ALLES KAPUTT");
                break;
        }
    });
}

// Gibt Fehlermeldungen für 10 Sekunden auf der Seite aus
function printAlert(msg) {
    alertComp.innerHTML = `<p class="text-danger">${msg}</p>`
    setTimeout(() => {
        hide(alertComp);
    }, 10000);
    show(alertComp);
}

function show(elem) {
    elem.style.display = "block";
}

function hide(elem) {
    elem.style.display = "none";
}