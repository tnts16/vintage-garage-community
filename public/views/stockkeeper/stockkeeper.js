//import axios, {AxiosResponse, AxiosError} from "axios"; // Diese Zeile vor der Ausführung auskommentieren!

let Staedte = ["Alle", "Berlin", "Hamburg", "München", "Gießen", "Frankfurt", "Köln"];
let Ausstattung = ["-", "beheizt", "klimatisiert", "UV-geschützt"];
let Marke = ["-","Porsche","BMW", "VW", "Audi"];

// HTML elements (add_service_to_space.html)
let divListOfPlaces = HTMLElement;
let divListOfServices = HTMLElement;
let divListOfBookings = HTMLElement;

document.addEventListener("DOMContentLoaded", () => {
    // initialise variables on first page load
    divListOfPlaces = document.getElementById("divListOfPlaces");
    divListOfServices = document.getElementById("divListOfServices");
    divListOfBookings = document.getElementById("divListOfBookings");

    adverts_list();
});

function adverts_list() {



    let BuchungVorhanden = false;
    let freierStVorhanden = false;
    divListOfPlaces.innerHTML = '<p class="card-text">Keine Stellplätze angelegt</p>'
    divListOfServices.innerHTML = '<p class="card-text">Keine Services angelegt</p>';
    divListOfPlaces.innerText = "";
    axios.get("/showOwnAdvert", { params: { lagerhalter: true } }).then(function (response) {

        console.log(response.data);
        
        //304 wenn Daten aus Cache
        if (response.status == 200 || response.status == 304) {


            try{
            for (let i = 0; i < response.data.length; i++) {
                if(response.data[i][0].buchung != null) {
                    // do nothing
                } else {
                    freierStVorhanden = true;
                    divListOfPlaces.innerHTML = divListOfPlaces.innerHTML + `<div class="col-xl-3 col-lg-4 col-md-6 col-sm-6 col-xs-12 mb-5">
                                <div class="card bg-skyblue">
                                    <div class="card-body text-center">
                                    <h5 class="card-title mt-0" style="background-color: #FFFFFF;">` + response.data[i][0].bezeichnung + `</h5>
                                        <p class="card-text">Standort: ` + Staedte[response.data[i][0].stadt] + `</p>
                                        <p class="card-text">Monatliche Kosten: ` + response.data[i][0].miete + `€</p>
                                        <a href="/edit_advert?id=` + response.data[i][0].stellplatzId + `" class="btn btn-secondary">Bearbeiten</a><br><br>
                                    </div>
                                </div>
                            </div>`
                }
                
                //List of Bookings
                if(response.data[i][0].buchung != null){
                    BuchungVorhanden = true;
                    let serviceString = "";
                    if(response.data[i][0].buchung.services.length === 0) {
                        serviceString = `<p class="card-text">keine Services gebucht</p>`;
                    } else {
                        for (let j = 0; j < response.data[i][0].buchung.services.length; j++) {
                            serviceString += `<p class="card-text">${response.data[i][0].buchung.services[j].bezeichnung}</p>`;
                        }
                    }
                    let fahrzeugString = "";
                    if(response.data[i][0].buchung.fahrzeug != null) {
                        fahrzeugString += `${Marke[response.data[i][0].buchung.fahrzeug.marke]} ${response.data[i][0].buchung.fahrzeug.bezeichnung} (${response.data[i][0].buchung.fahrzeug.baujahr})`
                    }
                    divListOfBookings.innerHTML = divListOfBookings.innerHTML + `<div class="col-xl-3 col-lg-4 col-md-6 col-sm-6 col-xs-12 mb-5">
                    <div class="card bg-skyblue">
                        <div class="card-body text-center">
                            <h5 class="card-title mt-0" style="background-color: #FFFFFF;">Stellplatz Buchung</h5>
                            <p class="card-text">Gebucht von: `+ response.data[i][0].buchung.einlagerer+`</p>
                            <p class="card-text">Fahrzeug: ${fahrzeugString}</p>`+ // response.data[i][0].buchung.fahrzeug.marke + ` ` + response.data[i][0].buchung.fahrzeug.bezeichnung
                            `
                            <p class="card-text"></p>
                            <h5 class="card-title mt-0" style="background-color: #FFFFFF;">Stellplatz: `+ response.data[i][0].bezeichnung + `</h5>
                            <p class="card-text">Standort: `+ Staedte[response.data[i][0].stadt] + `</p>
                            <p class="card-text">Monatliche Kosten: `+ response.data[i][0].miete + `€</p>
                            <h5 class="card-title mt-0" style="background-color: #FFFFFF;">Gebuchte Services</h5>
                            ${serviceString}
                        </div>
                    </div>
                </div>`

                }
                }
 
            }
            catch(e){
                console.log(e);
                divListOfPlaces.innerHTML = '<p class="card-text">Keine Stellplätze angelegt</p>'
            }
            if(BuchungVorhanden == false){
                divListOfBookings.innerHTML = '<p class="card-text">Keine Buchungen vorhanden</p>'
            }
            if(freierStVorhanden == false){
                divListOfPlaces.innerHTML = '<p class="card-text">Keine Stellplätze angelegt oder verfügbaren Stellplätze vorhanden</p>'
            }
            }
            if(response.data.length == 0){
                divListOfPlaces.innerHTML = '<p class="card-text">Keine Stellplätze angelegt</p>'
            }



            let ServiceList = "";
           try{
            for(let i = 0; i < response.data[0][1].length; i++){
                console.log("For "+i);
                ServiceList = ServiceList + `<div class="col-xl-3 col-lg-4 col-md-6 col-sm-6 col-xs-12 mb-5">
                                            <div class="card bg-skyblue">
                                                <div class="card-body text-center">
                                                    <h5 class="card-title mt-0" style="background-color: #FFFFFF;">`+response.data[0][1][i].bezeichnung+`</h5>
                                                    <p class="card-text">`+response.data[0][1][i].beschreibung+`</p>
                                                    <p class="card-text">Monatliche Kosten: `+ String(response.data[0][1][i].monatlKosten) +`€</p>
                                                </div>
                                            </div>
                                        </div>`;
            }
            divListOfServices.innerHTML = ServiceList;
            if(response.data[0][1].length == 0 || response.data[0][1] == undefined){
                divListOfServices.innerHTML = '<p class="card-text">Keine Services angelegt</p>';
                console.log("Keine Services angelegt");
            } 
        }
        catch{

        }

            

            
            

        });
}