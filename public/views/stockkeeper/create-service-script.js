// const axios = require("axios");

document.addEventListener("DOMContentLoaded", () => {
    document.getElementById("createServiceForm").addEventListener("submit", createService);
});

function createService(event) {
    event.preventDefault();
    console.log("hier!!!!");
    const target = event.currentTarget;
    const formData = new FormData(target);

    axios.post("/createService", {
        serviceBeschreibung: formData.get("beschreibung"),
        serviceBezeichnung: formData.get("bezeichnung"),
        serviceMonatlKosten: formData.get("monatlKosten")
    });
    //Weiterleiten Verzögerung
    setTimeout(function() {
        window.location.href = "/stockkeeper"
      }, 1000);
}
