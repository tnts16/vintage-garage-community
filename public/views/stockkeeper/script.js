// HTML elements (add_service_to_space.html)
let divListOfServices = null;
let divNewService = null;
let btnCreateService = null;

document.addEventListener("DOMContentLoaded", () => {
    // initialise variables on first page load
    divListOfServices = document.getElementById("divListOfServices");
    divNewService = document.getElementById("divNewService");
    btnCreateService = document.getElementById("btnCreateService");

    // event listener for permanent HTML elements
    btnCreateService.addEventListener("click", showNewServiceForm);
});

function submitNewService() {
    if (divNewService != null) {
        showNewServiceButton(); // hide new service form & show new service button (+ event listener)

        addNewServiceToList();
    } else {
        console.log("if you can read this, please handle the error... sorry");
    }
}

function showNewServiceForm() {
    if (divNewService != null) {
        // hide new service button & show new service form (+ event listener)
        divNewService.innerHTML =
            '<h2>Neuen Service Anlegen</h2>\n' +
            '<form>\n' +
            '<label for="inputName">Bezeichnung:</label><br>\n' +
            '<input type="text" id="inputName"><br>\n' +
            '<label for="inputBeschreibung">Beschreibung:</label><br>\n' +
            '<input type="text" id="inputBeschreibung"><br>\n' +
            '<label for="inputMietpreis">Monatl. Kosten (in €):</label><br>\n' +
            '<input type="number" id="inputMietpreis"><br>\n' +
            '</form><br>\n' +
            '<button type="submit" id="btnSubmitNewService" class="btn btn-secondary">Bestätigen</button>';
        document.getElementById("btnSubmitNewService").addEventListener("click", submitNewService)
    } else {
        console.log("if you can read this, please handle the error... sorry");
    }
}

function showNewServiceButton() {
    divNewService.innerHTML = '<button class="btn btn-secondary" id="btnCreateService">Neuen Service hinzufügen</button><br><br>';
    document.getElementById("btnCreateService").addEventListener("click", showNewServiceForm)
}

function addNewServiceToList() {
    divListOfServices.innerHTML =
        divListOfServices.innerHTML +
        '<div class="col-xl-3 col-lg-4 col-md-6 col-sm-6 col-xs-12 mb-5">' +
        '<div class="card bg-skyblue">' +
        '<div class="card-body text-center">' +
        '<h5 class="card-title mt-0" style="background-color: #FFFFFF;">Beispielservice</h5>' +
        '<p class="card-text">Dies ist ein beispielhafter Service.</p>' +
        '<p class="card-text">Monatliche Kosten: 49,99€</p>' +
        '<label for="cbService">Am Stellplatz verfügbar:</label>' +
        '<br>' +
        '<input class="btn btn-secondary" type="checkbox" id="cbService"/>' +
        '</div>' +
        '</div>' +
        '</div>' +
        '</div>'
}