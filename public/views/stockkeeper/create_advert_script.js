// const axios = require("axios");


document.addEventListener("DOMContentLoaded", () => {
    document.getElementById("createAdvertForm").addEventListener("submit", createAdvert);
});

function createAdvert(event) {
    event.preventDefault();
    console.log("hier!!!!");
    const target = event.currentTarget;
    const formData = new FormData(target);

    axios.post("/createAdvert", {
        advertBezeichnung: formData.get("inputBezeichnung"),
        advertStadt: formData.get("selectStadt"),
        advertMiete: formData.get("inputMietpreis"),
        advertServiceZeiten: formData.get("inputServiceZeiten"),
        advertZugangsZeiten: formData.get("inputZugangsZeiten"),
        advertAnzahl: formData.get("inputAnzahl"),
        advertAusstattung1: formData.get("ausstattung1"),
        advertAusstattung2: formData.get("ausstattung2"),
        advertAusstattung3: formData.get("ausstattung3")
    });
    //Weiterleiten Verzögerung
    setTimeout(function() {
        window.location.href = "/stockkeeper"
      }, 1000);
    
}
