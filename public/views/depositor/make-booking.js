// HTML elements (add_service_to_space.html)
let btnCreateCar = null;
let divNewCar = null;
let selectedcarid = null;
let stellplatzid = null;
let stellplatz_data = null;

document.addEventListener("DOMContentLoaded", () => {
    document.getElementById("createBooking").addEventListener("submit", createBooking);

    //Get URL Search Param, Stellplatz ID
    let params = new URLSearchParams(document.location.search);
    let id = params.get("id");
    stellplatzid = id;
    getDetail(id);
});
/*
function createCar() {
    if (btnCreateCar != null) {
        divNewCar.innerHTML =
            '<h2>Informationen zu ihrem Fahrzeug angeben</h2>\n' +
            '<form id="createnewCar">\n' +
            '<label for="selectMarke">Fahrzeugmarke:</label>' +
            '<select class="btn btn-secondary dropdown-toggle" id="selectMarke" name="selectMarke" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">' +
            '<option value=1 selected>Porsche</option>' +
            '<option value=2>BMW</option>' +
            '<option value=3>VW</option>' +
            '<option value=4>Audi</option>' +
            '<option value=5>Ford</option>' +
            '</select><br><br>' +
            '<label for="inputBeschreibung">Bezeichnung:</label><br>\n' +
            '<input type="text" id="inputBeschreibung" name="inputBeschreibung"><br>\n' +
            '<label for="inputBaujahr">Baujahr:</label><br>\n' +
            '<input type="number" value="1950" min="1850" max="2050" step="1" id="inputBaujahr" name="inputBaujahr"><br>\n' +
            '<br>' +
            '<button type="submit" id="btnSubmitNewCar" class="btn btn-secondary">Bestätigen</button>' +
            '</form><br>\n';
        //document.getElementById("btnSubmitNewCar").addEventListener("click", addNewCar)
        document.getElementById("createnewCar").addEventListener("submit", addNewCar);
    } else {
        console.log("if you can read this, please handle the error... sorry");
    }
}
*/

function addNewCar(event) {
    selectedcarid = 0;
    event.preventDefault();
    console.log("hier!!!!");
    const target = event.currentTarget;
    const formData = new FormData(target);
    //Post CAR
    try {
        axios.post("/createKFZ", {
            marke: formData.get("selectMarke"),
            beschreibung: formData.get("inputBeschreibung"),
            baujahr: formData.get("inputBaujahr"),
        }).then(function (response) {
            console.log(response.status);
            //Return ID von Fahrzeug und speicher
            selectedcarid = response.data;
            console.log(selectedcarid);
        });
    }
    catch {
        console.log("Fehler Post KFZ");
        selectedcarid = -1;
    }

}

function getDetail(id) {

    divListOfServices.innerHTML = "<p>Keine Services Verfügbar</p>";
    axios.get("/AdvertDetail", { params: { id: id } }).then(function (response) {

        console.log(response.data);
        //304 wenn Daten aus Cache
        if (response.status == 200 || response.status == 304) {

            stellplatz_data = response.data
            //Services 
            divListOfServices = document.getElementById("divListOfServices");
            divListOfServices.innerHTML = "";
            for (let i = 0; i < response.data[0][1].length; i++) {
                divListOfServices.innerHTML = divListOfServices.innerHTML +
                    `<div class="col-xl-3 col-lg-4 col-md-6 col-sm-6 col-xs-12 mb-5">
                <div class="card bg-skyblue">
                  <div class="card-body text-center">
                    <h5 class="card-title mt-0" style="background-color: #FFFFFF;">`+ response.data[0][1][i].bezeichnung + `</h5>
                    <p class="card-text">`+ response.data[0][1][i].beschreibung + `</p>
                    <p class="card-text">Monatliche Kosten: `+ response.data[0][1][i].monatlKosten + `€<br></p>
                    <label for="cbService">Service auswählen: </label>
                    <br>
                    <input class="btn btn-secondary" type="checkbox" id="cbService`+i+`"/>
                  </div>
                </div>
              </div>`
            }
            if (response.data[0][1].length == 0) {
                //Keine Services Verfügbar
                divListOfServices.innerHTML = "<p>Keine Services Verfügbar</p>";
            }
        }
        else{
            divListOfServices.innerHTML = "<p>Keine Services Verfügbar</p>";
        }

    });
}

//Keine Abfragen ob eingaben gültig
function createBooking(event){

    event.preventDefault();
    console.log(event);

    addNewCar(event);
    //Buchungsobjekt

    setTimeout(function() {
        let buchung = {
            datum: Date.now(),
            stellplatz: stellplatzid,
            lagerhalter: stellplatz_data[0][0].lagerhalter,
            services: [null],
            fahrzeug: selectedcarid
    
        };
    
        let cService = 0;
        for(let i=0; i< stellplatz_data[0][1].length; i++){
            checkbox = document.getElementById("cbService"+i);
            if(checkbox.checked == true){
                buchung.services[cService] = stellplatz_data[0][1][i].serviceID;
                cService++;
            }
        }
    
        console.log(buchung);
        //Post CAR
        try {
            //URL anpassen
            const res = axios.post('/createStellplatzBuchung', JSON.stringify(buchung), {
                headers: {
                    'Content-Type': 'application/json'
                }
            }).then(function (response) {
                console.log(response.status);
            });
        }
        catch {
            console.log("Fehler Post Buchung");
            selectedcarid = 0;
        }
    }, 700);



    //Zurück zu Übersichtsseite
    setTimeout(function() {
        window.location.href="depositor";
      }, 1000);
    

}