//import axios, {AxiosResponse, AxiosError} from "axios"; // Diese Zeile vor der Ausführung auskommentieren!

let Staedte = ["Alle", "Berlin", "Hamburg", "München", "Gießen", "Frankfurt", "Köln"];
let Ausstattung = ["-", "beheizt", "klimatisiert", "UV-geschützt"];

// HTML elements (add_service_to_space.html)
let ShowAdverts = HTMLElement;
let selectStadt = HTMLElement;

document.addEventListener("DOMContentLoaded", () => {
    // initialise variables on first page load
    //ShowAdverts = document.getElementById("divShowAdverts");


    //Get URL Search Param
    let params = new URLSearchParams(document.location.search);
    let id = params.get("id");

    console.log(id);

    //Buchen URL Anpassen
    
    but_booking = document.getElementById("but_booking");
    but_booking.href =  "/make_booking?id="+id;

    getDetail(id);
});

function getDetail(id) {

    //Noch richtige API URL eintragen
    axios.get("/AdvertDetail", { params: { id: id } }).then(function (response) {

        console.log(response.data);
        //304 wenn Daten aus Cache
        if (response.status == 200 || response.status == 304) {

            //Bezeichnung
            tdBezeichnung = document.getElementById("tdBezeichnung");
            tdBezeichnung.innerHTML = response.data[0][0].bezeichnung;
            //Standort
            tdStandort = document.getElementById("tdStandort");
            tdStandort.innerHTML = Staedte[response.data[0][0].stadt];
            //Kosten
            tdKosten = document.getElementById("tdKosten");
            tdKosten.innerHTML = response.data[0][0].miete + "€";
            //Servicezeiten
            tdServicezeiten = document.getElementById("tdServicezeiten");
            tdServicezeiten.innerHTML = response.data[0][0].serviceZeiten;
            //Zugangszeiten
            tdZugangszeiten = document.getElementById("tdZugangszeiten");
            tdZugangszeiten.innerHTML = response.data[0][0].zugangsZeiten;

            //Ausstattung
            tdAusstattung = document.getElementById("tdAusstattung");
            tdAusstattung.innerHTML = "";
            for (let i = 0; i < response.data[0][0].ausstattung.length; i++) {
                tdAusstattung.innerHTML = tdAusstattung.innerHTML + Ausstattung[response.data[0][0].ausstattung[i]] + '<br>'
            }

            //Services 
            divListOfServices = document.getElementById("divListOfServices");
            divListOfServices.innerHTML = "";
            for (let i = 0; i < response.data[0][1].length; i++) {
                divListOfServices.innerHTML = divListOfServices.innerHTML + 
                `<div class="col-xl-3 col-lg-4 col-md-6 col-sm-6 col-xs-12 mb-5">
                    <div class="card bg-skyblue">
                        <div class="card-body text-center">
                            <h5 class="card-title mt-0" style="background-color: #FFFFFF;">`+ response.data[0][1][i].bezeichnung +`</h5>
                            <p class="card-text">`+ response.data[0][1][i].beschreibung +`</p>
                            <p class="card-text">Monatliche Kosten: `+ response.data[0][1][i].monatlKosten + `€<br></p>
                        </div>
                    </div>
                </div>`
            }
            if(response.data[0][1].length == 0){
                //Keine Services Verfügbar
                divListOfServices.innerHTML = "<p>Keine Services Verfügbar</p>";
            }

        }

    });
}
