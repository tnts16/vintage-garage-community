//import axios, {AxiosResponse, AxiosError} from "axios"; // Diese Zeile vor der Ausführung auskommentieren!

let Staedte = ["Alle", "Berlin", "Hamburg", "München", "Gießen", "Frankfurt", "Köln"];
let Ausstattung = ["-", "beheizt", "klimatisiert", "UV-geschützt"];

// HTML elements (add_service_to_space.html)
let ShowAdverts = HTMLElement;
let selectStadt = HTMLElement;

document.addEventListener("DOMContentLoaded", () => {
    // initialise variables on first page load
    ShowAdverts = document.getElementById("divShowAdverts");

    selectStadt = document.getElementById("selectStadt");
    selectStadt.addEventListener("change", adverts_list);
    adverts_list();
});

function adverts_list() {
    console.log(selectStadt.value);
    ShowAdverts.innerText = "";
    let Stellplatzverfügbar = false;    //true wenn min ein Stellplatz angezeigt wird
    axios.get("/showAdvertlist", { params: { stadt: selectStadt.value } }).then(function (response) {
        try {
            console.log(response.data);
            //  console.log(response.data.stellplaetze);
            //  console.log(response.data.services);

            //304 wenn Daten aus Cache
            if (response.status == 200 || response.status == 304) {


                for (let i = 0; i < response.data.length; i++) {

                    if (response.data[i][0].buchung == null){
                        Stellplatzverfügbar = true;
                        //Service Liste
                        let ServiceList = "";
                        let ii = 0;
                        try {
                            while (response.data[i][1][ii] != undefined) {
                                if (response.data[i][1][ii] == undefined) {
                                    break;
                                }
                                ServiceList = ServiceList + '<p class="card-text">' + String(response.data[i][1][ii].bezeichnung) + '    (' + String(response.data[i][1][ii].monatlKosten) + '&#8364) </p>';
                                ii++
                            }
                            if (response.data[i][1] == undefined) {
                                ServiceList = '<p class="card-text">Es werden keine Services angeboten</p>';
                            }

                        }
                        catch {
                            console.log("Error Service Liste: " + i);
                            ServiceList = '<p class="card-text">Es werden keine Services angeboten</p>';
                        }

                        //Ausstattungsliste
                        let AusstattungListe = "";
                        for (let ii = 0; ii < response.data[i][0].ausstattung.length; ii++) {
                            if (ii > 0) {
                                AusstattungListe = AusstattungListe + ", "
                            }
                            AusstattungListe = AusstattungListe + Ausstattung[response.data[i][0].ausstattung[ii]];
                        }
                        if (response.data[i][0].ausstattung.length == 0) {
                            AusstattungListe = "-"
                        }
                        const row = document.createElement("tr");
                        row.innerHTML = `<div class="col-xl-3 col-lg-4 col-md-6 col-sm-6 col-xs-12 mb-5 card w-100">
                                    <div class="card bg-skyblue">
                                        <div class="card-body text-center">
                                            <h5 class="card-title mt-0" style="background-color: #FFFFFF;">`+ response.data[i][0].bezeichnung + `</h5>
                                            <p class="card-text">Standort: `+ Staedte[response.data[i][0].stadt] + `</p>
                                            <p class="card-text">Monatliche Kosten: `+ response.data[i][0].miete + `€</p>
                                            <p class="card-text">Ausstattung: `+ AusstattungListe + `</p>
                                            <h5 class="card-title mt-0" style="background-color: #FFFFFF;">Angebotene Services</h5>
                                            <p class="card-text">`+ ServiceList + `</p>
                                            <a href="/advert_detail?id=`+ response.data[i][0].stellplatzID + `" class="btn btn-secondary">Details</a><br><br>
                                        </div>
                                    </div>
                                </div>`;

                        ShowAdverts.appendChild(row);
                    }
                }
            }
            if (response.data.length == 0 || response.data == undefined || Stellplatzverfügbar == false) {
                ShowAdverts.innerText = "Keine Stellplätze gefunden";
            }
        }
        catch {
            ShowAdverts.innerText = "Keine Stellplätze gefunden";
            console.log("Error Stellplatzliste")
        }

    });
}