//import axios, {AxiosResponse, AxiosError} from "axios"; // Diese Zeile vor der Ausführung auskommentieren!

let Staedte = ["Alle", "Berlin", "Hamburg", "München", "Gießen", "Frankfurt", "Köln"];
let Ausstattung = ["-", "beheizt", "klimatisiert", "UV-geschützt"];
let Marke = ["-","Porsche","BMW", "VW", "Audi"];


// HTML elements (add_service_to_space.html)

let divListOfBookings = HTMLElement;

document.addEventListener("DOMContentLoaded", () => {
    // initialise variables on first page load
    divListOfBookings = document.getElementById("divListOfBookings");

    booking_list();
});

function booking_list() {

    divListOfBookings.innerText = "";
    axios.get("/showOwnBuchung", { params: { einlagerer: true } }).then(function (response) {

        console.log(response.data);
        
        //304 wenn Daten aus Cache
        if (response.status == 200 || response.status == 304) {


            for (let i = 0; i < response.data.length; i++) {

                let serviceString = "";
                if(response.data[i][0].buchung.services.length === 0) {
                    serviceString = `<p class="card-text">keine Services gebucht</p>`;
                } else {
                    for (let j = 0; j < response.data[i][0].buchung.services.length; j++) {
                        serviceString += `<p class="card-text">${response.data[i][0].buchung.services[j].bezeichnung}</p>`;
                    }
                }

                let fahrzeugString = "";
                if(response.data[i][0].buchung.fahrzeug != null) {
                    fahrzeugString += `${Marke[response.data[i][0].buchung.fahrzeug.marke]} ${response.data[i][0].buchung.fahrzeug.bezeichnung} (${response.data[i][0].buchung.fahrzeug.baujahr})`
                }

                //List of Bookings
                if(response.data[i][0].buchung != null){
                    divListOfBookings.innerHTML = divListOfBookings.innerHTML + `<div class="col-xl-3 col-lg-4 col-md-6 col-sm-6 col-xs-12 mb-5">
                    <div class="card bg-skyblue">
                        <div class="card-body text-center">
                            <h5 class="card-title mt-0" style="background-color: #FFFFFF;">Stellplatz Gebucht</h5>
                            <p class="card-text">Gebucht bei: `+ response.data[i][0].buchung.lagerhalter+`</p>
                            <p class="card-text">Fahrzeug: ${fahrzeugString}</p>
                            <p class="card-text"></p>
                            <h5 class="card-title mt-0" style="background-color: #FFFFFF;">Stellplatz: `+ response.data[i][0].bezeichnung + `</h5>
                            <p class="card-text">Standort: `+ Staedte[response.data[i][0].stadt] + `</p>
                            <p class="card-text">Monatliche Kosten: `+ response.data[i][0].miete + `€</p>
                            <h5 class="card-title mt-0" style="background-color: #FFFFFF;">Gebuchte Services</h5>
                            ${serviceString}
                        </div>
                    </div>
                </div>`

                }
                }
 

            }
            if(response.data.length == 0){
                divListOfBookings.innerHTML = '<p class="card-text">Keine Stellplätze gebucht</p>'
            }
  

        });
}